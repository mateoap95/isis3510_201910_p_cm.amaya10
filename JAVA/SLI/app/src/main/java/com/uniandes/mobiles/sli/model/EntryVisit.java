package com.uniandes.mobiles.sli.model;

import com.google.gson.annotations.SerializedName;

public class EntryVisit {
    @SerializedName("id")
    private Integer id;
    @SerializedName("visit_user")
    private String visit_user;
    @SerializedName("visit_word")
    private String visit_word;

    public EntryVisit(Integer id, String visit_user, String visit_word) {
        this.id = id;
        this.visit_user = visit_user;
        this.visit_word = visit_word;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getVisit_user() {
        return visit_user;
    }

    public void setVisit_user(String visit_user) {
        this.visit_user = visit_user;
    }

    public String getVisit_word() {
        return visit_word;
    }

    public void setVisit_word(String visit_word) {
        this.visit_word = visit_word;
    }
}
