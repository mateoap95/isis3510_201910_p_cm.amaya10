package com.uniandes.mobiles.sli.model;

import com.google.gson.annotations.SerializedName;

public class CSLDetection {
    @SerializedName("id")
    private Integer id;
    @SerializedName("csl_user")
    private String csl_user;
    @SerializedName("csl_class")
    private String csl_class;
    @SerializedName("csl_time")
    private long csl_time;
    @SerializedName("csl_prediction")
    private float csl_prediction;

    public CSLDetection(Integer id, String csl_user, String csl_class, long csl_time, float csl_prediction) {
        this.id = id;
        this.csl_user = csl_user;
        this.csl_class = csl_class;
        this.csl_time = csl_time;
        this.csl_prediction = csl_prediction;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCsl_user() {
        return csl_user;
    }

    public void setCsl_user(String csl_user) {
        this.csl_user = csl_user;
    }

    public String getCsl_class() {
        return csl_class;
    }

    public void setCsl_class(String csl_class) {
        this.csl_class = csl_class;
    }

    public long getCsl_time() {
        return csl_time;
    }

    public void setCsl_time(long csl_time) {
        this.csl_time = csl_time;
    }

    public float getCsl_prediction() {
        return csl_prediction;
    }

    public void setCsl_prediction(float csl_prediction) {
        this.csl_prediction = csl_prediction;
    }
}
