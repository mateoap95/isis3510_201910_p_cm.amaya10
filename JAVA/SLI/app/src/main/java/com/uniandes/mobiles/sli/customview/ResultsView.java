package com.uniandes.mobiles.sli.customview;

import com.uniandes.mobiles.sli.Detector;

import java.util.List;

public interface ResultsView {
    public void setResults(final List<Detector.Recognition> results);
}
