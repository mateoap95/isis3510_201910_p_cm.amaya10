package com.uniandes.mobiles.sli;
import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.google.firebase.analytics.FirebaseAnalytics;
import com.uniandes.mobiles.sli.data.DictionaryContract;
import com.uniandes.mobiles.sli.data.DictionaryDBHelper;
import com.uniandes.mobiles.sli.data.EntriesCursorAdapter;


/**
 * A simple {@link Fragment} subclass.
 * Use the {@link EntryFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class EntryFragment extends Fragment {

    private static final int REQUEST_UPDATE_DELETE_LAWYER = 5;
    private DictionaryDBHelper mDictionaryDbHelper;
    private ListView mEntryList;
    private EntriesCursorAdapter mEntryAdapter;

    private String userID;

    public EntryFragment() {
        // Required empty public constructor
    }

    public static EntryFragment newInstance() {
        return new EntryFragment();
    }

    private void showDetailScreen(String entryId, String entryWord) {
        Intent intent = new Intent(getActivity(), EntryDetailActivity.class);
        intent.putExtra(DictionaryActivity.EXTRA_ENTRY_ID, entryId);
        intent.putExtra(DictionaryActivity.EXTRA_ENTRY_WORD, entryWord);
        intent.putExtra(LoginActivity.EXTRA_USER_ID, userID);
        startActivityForResult(intent, REQUEST_UPDATE_DELETE_LAWYER);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View root = inflater.inflate(R.layout.fragment_entry, container, false);

        mEntryList = (ListView) root.findViewById(R.id.entries_list);
        mEntryAdapter = new EntriesCursorAdapter(getActivity(), null);
        mEntryList.setAdapter(mEntryAdapter);

        mEntryList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Cursor currentItem = (Cursor) mEntryAdapter.getItem(i);
                String currentEntryId = currentItem.getString(
                        currentItem.getColumnIndex(DictionaryContract.DicEntry.ID));
                String currentEntryWord = currentItem.getString(
                        currentItem.getColumnIndex(DictionaryContract.DicEntry.WORD));
                showDetailScreen(currentEntryId,currentEntryWord);
            }
        });

        mDictionaryDbHelper = new DictionaryDBHelper(getActivity());

        loadEntries();

        return root;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (Activity.RESULT_OK == resultCode) {
            switch (requestCode) {
                case REQUEST_UPDATE_DELETE_LAWYER:
                    loadEntries();
                    break;
            }
        }
    }

    private void loadEntries(){
    new EntriesLoadTask().execute();
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }

    private class EntriesLoadTask extends AsyncTask<Void, Void, Cursor> {

        @Override
        protected Cursor doInBackground(Void... voids) {
            return mDictionaryDbHelper.getAllEntries();
        }

        @Override
        protected void onPostExecute(Cursor cursor) {
            if (cursor != null && cursor.getCount() > 0) {
                mEntryAdapter.swapCursor(cursor);
            } else {
                cursor.close();
            }
        }
    }
}
