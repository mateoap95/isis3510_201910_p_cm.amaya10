package com.uniandes.mobiles.sli;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;

import com.crashlytics.android.Crashlytics;
import com.flurry.android.FlurryAgent;
import com.google.firebase.FirebaseApp;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.treebo.internetavailabilitychecker.InternetAvailabilityChecker;

import io.fabric.sdk.android.Fabric;

public class LauncherActivity extends AppCompatActivity {

    private FirebaseAuth mAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_launcher);
        setTheme(R.style.AppTheme);
        InternetAvailabilityChecker.init(this);
        new FlurryAgent.Builder()
                .withLogEnabled(true)
                .build(this, "TBG4P9ZCY94HGZ5N73MJ");
        Fabric.with(this, new Crashlytics());
        mAuth = FirebaseAuth.getInstance();
    }

    @Override
    protected void onStart() {
        super.onStart();
        FirebaseApp.initializeApp(this);
        FirebaseUser currentUser = mAuth.getCurrentUser();
        updateUI(currentUser);
    }

    private void updateUI(FirebaseUser user){
        Intent activityIntent;
        if(user != null){
            activityIntent = new Intent(this, MainActivity.class);
            activityIntent.putExtra("UserID",user.getUid());
        }else{
            activityIntent = new Intent(this, LoginActivity.class);
        }
        startActivity(activityIntent);
        finish();
    }
}
