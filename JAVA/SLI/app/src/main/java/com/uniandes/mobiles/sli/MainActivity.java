package com.uniandes.mobiles.sli;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.flurry.android.FlurryAgent;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.uniandes.mobiles.sli.data.DictionaryEntry;

import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    @BindView(R.id.button_conversation_main)
    Button btn_chat;
    @BindView(R.id.button_signs_asl_main)
    Button btn_asl;
    @BindView(R.id.button_signs_csl_main)
    Button btn_csl;
    @BindView(R.id.button_dictionary_main)
    Button btn_dic;
    @BindView(R.id.button_detector_csl_main)
    Button btn_detector;

    private FirebaseAuth mAuth;
    private String userID;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        mAuth = FirebaseAuth.getInstance();

        btn_chat.setOnClickListener(this);
        btn_asl.setOnClickListener(this);
        btn_csl.setOnClickListener(this);
        btn_dic.setOnClickListener(this);
        btn_detector.setOnClickListener(this);

        Intent intent = getIntent();
        userID = intent.getStringExtra(LoginActivity.EXTRA_USER_ID);
        if (userID == null || userID.isEmpty()) {
            userID = mAuth.getCurrentUser().getUid();
            if (userID == null || userID.isEmpty()) {
                userID = "002";
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.menu_sign_out) {
            mAuth.signOut();
            Intent activityIntent = new Intent(this, LauncherActivity.class);
            startActivity(activityIntent);
            finish();
        }
        return true;
    }

    @Override
    public void onClick(View view) {
        Intent activityIntent;
        int i = view.getId();
        if (i == R.id.button_conversation_main) {
            activityIntent = new Intent(this, ChatActivity.class);
            activityIntent.putExtra(LoginActivity.EXTRA_USER_ID, userID);
            startActivity(activityIntent);
        } else if (i == R.id.button_signs_asl_main) {
            activityIntent = new Intent(this, ASLActivity.class);
            activityIntent.putExtra(LoginActivity.EXTRA_USER_ID, userID);
            startActivity(activityIntent);
        } else if (i == R.id.button_signs_csl_main) {
            activityIntent = new Intent(this, CSLActivity.class);
            activityIntent.putExtra(LoginActivity.EXTRA_USER_ID, userID);
            startActivity(activityIntent);
        } else if (i == R.id.button_dictionary_main) {
            activityIntent = new Intent(this, DictionaryActivity.class);
            activityIntent.putExtra(LoginActivity.EXTRA_USER_ID, userID);
            startActivity(activityIntent);
        } else if (i == R.id.button_detector_csl_main) {
            activityIntent = new Intent(this, DetectorActivity.class);
            activityIntent.putExtra(LoginActivity.EXTRA_USER_ID, userID);
            startActivity(activityIntent);
        }
    }
}
