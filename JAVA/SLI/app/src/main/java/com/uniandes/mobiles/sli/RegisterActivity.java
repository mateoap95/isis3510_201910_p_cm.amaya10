package com.uniandes.mobiles.sli;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.DialogFragment;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.text.format.DateUtils;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.flurry.android.FlurryAgent;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.treebo.internetavailabilitychecker.InternetConnectivityListener;
import com.uniandes.mobiles.sli.model.User;
import com.uniandes.mobiles.sli.network.APIService;
import com.uniandes.mobiles.sli.network.APIUtils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RegisterActivity extends AppCompatActivity implements
        View.OnClickListener, InternetConnectivityListener {

    @BindView(R.id.txtName)  EditText et_name;
    @BindView(R.id.txtLastName) EditText et_lastname;
    @BindView(R.id.txtEmail) EditText et_email;
    @BindView(R.id.txtPassword) EditText et_password;
    @BindView(R.id.txtPasswordConfirm) EditText et_password_check;
    @BindView(R.id.txtDate) EditText et_date;
    @BindView(R.id.spinnerCondition) Spinner spinnerCondition;
    @BindView(R.id.spinnerSex) Spinner spinnerSex;
    @BindView(R.id.txtSignIn) TextView lblSignIn;
    @BindView(R.id.progressBarRegister) ProgressBar progressBar;
    @BindView(R.id.button_signup) Button btnSignup;
    
    private FirebaseAuth mAuth;
    private FirebaseAuth.AuthStateListener mAuthListener;
    private FirebaseDatabase database;
    private DatabaseReference firebaseDB;
    private FirebaseAnalytics mFirebaseAnalytics;

    private static final String TAG = "Register Activity";

    private String[] arraySpinnerConditions;
    private String[] arraySpinnerSex;

    private UserDB newUser;
    private APIService mAPIService;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        ButterKnife.bind(this);
        
        lblSignIn.setOnClickListener(this);
        et_date.setOnClickListener(this);
        btnSignup.setOnClickListener(this);

        mAPIService = APIUtils.getAPIService();
        
        arraySpinnerConditions = new String[]{
                "Condicion", "Sordera Parcial", "Sordera Total", "Sordera Unilateral", "Sordomudo", "Ninguna de las anteriores"
        };

        arraySpinnerSex = new String[]{
                "Sexo", "Hombre", "Mujer", "Otro"
        };

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, arraySpinnerConditions);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerCondition.setAdapter(adapter);

        ArrayAdapter<String> adapterSex = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, arraySpinnerSex);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerSex.setAdapter(adapterSex);

        mAuth = FirebaseAuth.getInstance();
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);
        database = FirebaseDatabase.getInstance();
        firebaseDB = database.getReference();

        mAuthListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                FirebaseUser user = firebaseAuth.getCurrentUser();

                if (user != null && newUser!=null) {
                    firebaseDB.child("users").child(user.getUid()).setValue(newUser);
                    Intent intent = new Intent(RegisterActivity.this, MainActivity.class);
                    intent.putExtra("UserID",user.getUid());
                    startActivity(intent);
                    finish();
                } else {
                    // User is signed out
                    Log.d(TAG, "onAuthStateChanged:signed_out");
                }
            }
        };

    }

    @Override
    protected void onStart() {
        super.onStart();
        mAuth.addAuthStateListener(mAuthListener);
    }

    @Override
    public void onStop() {
        super.onStop();
        if (mAuthListener != null) {
            mAuth.removeAuthStateListener(mAuthListener);
        }

    }

    private void showDatePickerDialog() {
        DatePickerFragment newFragment = DatePickerFragment.newInstance(new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int year, int month, int day) {
                // +1 because january is zero
                final String selectedDate = day + "/" + (month + 1) + "/" + year;
                Log.i("Date", selectedDate);
                if (!selectedDate.isEmpty() && selectedDate != null) {
                    et_date.setText(selectedDate.toString());
                }
            }
        });
        newFragment.show(this.getSupportFragmentManager(), "datePicker");
    }

    private void signUp() {

        final String name = et_name.getText().toString();
        final String last_name = et_lastname.getText().toString();
        final String email = et_email.getText().toString();
        final String password = et_password.getText().toString();
        final String password_check = et_password_check.getText().toString();
        final String condition = spinnerCondition.getSelectedItem().toString();
        final String sex = spinnerSex.getSelectedItem().toString();
        final String date = et_date.getText().toString();

        if (TextUtils.isEmpty(email) || TextUtils.isEmpty(date) || TextUtils.isEmpty(name) || TextUtils.isEmpty(last_name)) {
            Toast.makeText(getApplicationContext(), "Falta algun dato", Toast.LENGTH_SHORT).show();
            return;
        } else if (TextUtils.isEmpty(password) || TextUtils.isEmpty(password_check)) {
            Toast.makeText(getApplicationContext(), "Falta alguna de las contraseñas", Toast.LENGTH_SHORT).show();
            return;
        } else if (!TextUtils.equals(password, password_check)) {
            Toast.makeText(getApplicationContext(), "Contraseñas no coinciden", Toast.LENGTH_SHORT).show();
            return;
        } else if (password.length() < 8) {
            Toast.makeText(getApplicationContext(), "Contraseña debe tener minimo ocho caracteres.", Toast.LENGTH_SHORT).show();
            return;
        } else if (TextUtils.equals(condition, arraySpinnerConditions[0])) {
            Toast.makeText(getApplicationContext(), "Elija una condicion valida.", Toast.LENGTH_SHORT).show();
            return;
        }else if (TextUtils.equals(sex, arraySpinnerSex[0])) {
            Toast.makeText(getApplicationContext(), "Elija un sexo valido.", Toast.LENGTH_SHORT).show();
            return;
        }

        Bundle bundle = new Bundle();
        bundle.putString("condition_type",condition);
        bundle.putString("sex",sex);
        mFirebaseAnalytics.logEvent("new_user", bundle);

        Map<String, String> signUpParams = new HashMap<String, String>();
        signUpParams.put("Condition", condition);
        signUpParams.put("Sex", sex);
        FlurryAgent.logEvent("SignUp", signUpParams);

        mAuth.createUserWithEmailAndPassword(email, password)
                .addOnCompleteListener(RegisterActivity.this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        progressBar.setVisibility(View.GONE);
                        if (task.isSuccessful()) {
                            // Sign in success, update UI with the signed-in user's information
                            newUser = new UserDB(name, last_name, email, date, condition,sex);
                            String uid = task.getResult().getUser().getUid();
                            Date date1 = new Date();
                            try {
                                date1 =new SimpleDateFormat("dd/MM/yyyy").parse(date);
                            } catch (ParseException e) {
                               Log.e(TAG,e.getLocalizedMessage());
                            }
                            User newUserRF = new User(9,name,email,uid,last_name,condition,sex,date1);
                            saveUser(newUserRF);
                            Log.d(TAG, "createUserWithEmail:success");
                        } else {
                            // If sign in fails, display a message to the user.
                            Log.w(TAG, "createUserWithEmail:failure", task.getException());
                        }
                    }
                });
    }

    public void saveUser(User user) {
        mAPIService.saveUser(user).enqueue(new Callback<User>() {
            @Override
            public void onResponse(Call<User> call, Response<User> response) {

                if(response.isSuccessful()) {
                    Log.i(TAG, "post submitted to API." + response.body().toString());
                }
            }

            @Override
            public void onFailure(Call<User> call, Throwable t) {
                Log.e(TAG, "Unable to submit post to API.");
            }
        });
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.txtDate:
                showDatePickerDialog();
                break;
            case R.id.txtSignIn:
                Intent intent = new Intent(RegisterActivity.this, LoginActivity.class);
                startActivity(intent);
                break;
            case R.id.button_signup:

                progressBar.setVisibility(View.VISIBLE);
                signUp();
                break;
        }
    }

    @Override
    public void onInternetConnectivityChanged(boolean isConnected) {

    }

    public class UserDB {

        private String name;
        private String lastname;
        private Date birthdate;
        private String email;
        private String condition;
        private String sex;

        public UserDB() {
            // Default constructor required for calls to DataSnapshot.getValue(User.class)
        }

        public UserDB(String name, String lastname, String email, String date, String condition,String sex) {
            this.name = name;
            this.lastname = lastname;
            this.email = email;
            this.condition = condition;
            this.sex = sex;
            SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
            try {
                this.birthdate = formatter.parse(date);
            } catch (ParseException e) {
                Log.e(TAG, e.getMessage());
            }

        }


    }

}
