package com.uniandes.mobiles.sli;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ChatAdapter extends ArrayAdapter<ChatMessage> {

    static class MessageViewHolder {
        @BindView(R.id.message_body) TextView messageBody;

        public MessageViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }

    ArrayList<ChatMessage> mAdapterData;

    public ChatAdapter(Context context, ArrayList<ChatMessage> messages) {
        super(context,0,messages);
        this.mAdapterData = messages;
    }


    @Override
    public void add(@Nullable ChatMessage object) {
        super.add(object);
    }

    @Override
    public View getView(int i, View convertView, ViewGroup viewGroup) {
        MessageViewHolder holder;
        ChatMessage message = getItem(i);

        if (convertView == null) {
            LayoutInflater inflater = LayoutInflater.from(getContext());
            if (message.MSG_TYPE_SENT.equals(message.getMsgType())) {
                convertView = LayoutInflater.from(getContext()).inflate(R.layout.my_message, viewGroup, false);
            }
            else if(message.MSG_TYPE_RECEIVED.equals(message.getMsgType())){
                convertView = LayoutInflater.from(getContext()).inflate(R.layout.their_message, viewGroup, false);
            }
            holder = new MessageViewHolder(convertView);
            convertView.setTag(holder);

        }else {
            holder = (MessageViewHolder) convertView.getTag();
        }
        holder.messageBody.setText(message.getMsgContent());
        return convertView;
    }

    public ArrayList<ChatMessage> getList() {
        return new ArrayList<ChatMessage>();
    }
}

