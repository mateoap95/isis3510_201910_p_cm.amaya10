package com.uniandes.mobiles.sli.model;

import com.google.gson.annotations.SerializedName;

import java.util.Date;

public class Survey {
    @SerializedName("id")
    private Integer id;
    @SerializedName("survey_user")
    private String survey_user;
    @SerializedName("survey_class")
    private String survey_class;
    @SerializedName("survey_tp")
    private int survey_tp;
    @SerializedName("survey_confidence")
    private Float survey_confidence;
    @SerializedName("survey_model")
    private String survey_model;

    public Survey(Integer id, String survey_user, String survey_class, int survey_tp, Float survey_confidence, String survey_model) {
        this.id = id;
        this.survey_user = survey_user;
        this.survey_class = survey_class;
        this.survey_tp = survey_tp;
        this.survey_confidence = survey_confidence;
        this.survey_model = survey_model;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getSurvey_user() {
        return survey_user;
    }

    public void setSurvey_user(String survey_user) {
        this.survey_user = survey_user;
    }

    public String getSurvey_class() {
        return survey_class;
    }

    public void setSurvey_class(String survey_class) {
        this.survey_class = survey_class;
    }

    public int getSurvey_tp() {
        return survey_tp;
    }

    public void setSurvey_tp(int survey_tp) {
        this.survey_tp = survey_tp;
    }

    public Float getSurvey_confidence() {
        return survey_confidence;
    }

    public void setSurvey_confidence(Float survey_confidence) {
        this.survey_confidence = survey_confidence;
    }

    public String getSurvey_model() {
        return survey_model;
    }

    public void setSurvey_model(String survey_model) {
        this.survey_model = survey_model;
    }
}
