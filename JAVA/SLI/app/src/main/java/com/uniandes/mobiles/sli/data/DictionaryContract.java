package com.uniandes.mobiles.sli.data;

import android.provider.BaseColumns;

public class DictionaryContract {

    public static abstract class DicEntry implements BaseColumns {
        public static final String TABLE_NAME ="dictionary";

        public static final String ID = "id";
        public static final String WORD = "word";
        public static final String DEFINITION = "definition";
        public static final String SIGN_URL = "signUrl";
        public static final String DEFINITION_URL = "definitionUrl";
        public static final String EXAMPLE = "example";
        public static final String EXAMPLE_URL = "exampleUrl";
        public static final String LETTER = "letter";
        public static final String CM = "cm";
        public static final String TRANSLATION_URL = "translationURL";
    }
}
