package com.uniandes.mobiles.sli;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;


import android.Manifest;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.provider.Settings;
import android.speech.tts.TextToSpeech;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.Toast;

import com.flurry.android.FlurryAgent;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.maxwell.speechrecognition.OnSpeechRecognitionListener;
import com.maxwell.speechrecognition.SpeechRecognition;
import com.uniandes.mobiles.sli.model.ChatUsage;
import com.uniandes.mobiles.sli.model.Usage;
import com.uniandes.mobiles.sli.network.APIService;
import com.uniandes.mobiles.sli.network.APIUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ChatActivity extends AppCompatActivity implements OnSpeechRecognitionListener {

    private static final String TAG = "ChatActivity";
    private static final String STATE_LIST = "State Adapter Data";
    private static final String FEATURE_NAME = "Chat";

    @BindView(R.id.et_tts) EditText et_tts;
    @BindView(R.id.button_tts) ImageButton btn_tts;
    @BindView(R.id.chatList) ListView messagesView;
    @BindView(R.id.fab_speak) FloatingActionButton btnSpeak;

    private FirebaseAnalytics mFirebaseAnalytics;
    private String userID;
    private long startTime;
    private long startTimeSST;

    private ChatAdapter messageAdapter;
    private TextToSpeech textToSpeech;
    private ArrayList<ChatMessage> messages;
    private SpeechRecognition speechRecognition;

    private Map<String, String> usageParams;
    private APIService mAPIService;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        ButterKnife.bind(this);

        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);
        userID = getIntent().getStringExtra(LoginActivity.EXTRA_USER_ID);

        mAPIService = APIUtils.getAPIService();

        if (savedInstanceState != null) {
            messages = savedInstanceState.getParcelableArrayList(STATE_LIST);
            if(messages == null){
                messages = new ArrayList<ChatMessage>();
            }
            messageAdapter = new ChatAdapter(this, messages);
        } else {
            messages = new ArrayList<ChatMessage>();
            messageAdapter = new ChatAdapter(this, messages);
        }
        messagesView.setAdapter(messageAdapter);

        final Locale locSpanish = new Locale("spa", "MEX");
        textToSpeech = new TextToSpeech(getApplicationContext(), new TextToSpeech.OnInitListener() {
            @Override
            public void onInit(int status) {
                if (status != TextToSpeech.ERROR) {
                    textToSpeech.setLanguage(locSpanish);
                }
            }
        });

        speechRecognition = new SpeechRecognition(this);
        speechRecognition.handleAudioPermissions(false);
        speechRecognition.setSpeechRecognitionListener(this);

        btn_tts.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final String toSpeak = et_tts.getText().toString();
                if(!toSpeak.isEmpty()) {
                    final long startTTS = System.currentTimeMillis();
                    textToSpeech.speak(toSpeak, TextToSpeech.QUEUE_FLUSH, null);
                    final ChatMessage message = new ChatMessage(ChatMessage.MSG_TYPE_SENT, toSpeak);
                    messageAdapter.add(message);
                    final long elapsedTime = System.currentTimeMillis() - startTTS;
                    ChatUsage usage = new ChatUsage(9,userID,"TTS",elapsedTime,toSpeak.length());
                    sendChatUsage(usage);
                }
                et_tts.getText().clear();
            }
        });


        btnSpeak.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startTimeSST = System.currentTimeMillis();
                speechRecognition.startSpeechRecognition();
            }
        });

        checkPermission();
    }

    @Override
    protected void onStart() {
        startTime = System.currentTimeMillis();
        usageParams = new HashMap<String, String>();
        usageParams.put("FeatureName", FEATURE_NAME);
        usageParams.put("UserID", userID);
        FlurryAgent.logEvent("Usage", usageParams,true);
        super.onStart();
    }

    public void onPause() {
        if (textToSpeech != null) {
            textToSpeech.stop();
            textToSpeech.shutdown();
        }
        if(speechRecognition!=null){
            speechRecognition.stopSpeechRecognition();
        }
        measureTime();
        super.onPause();
    }


    @Override
    public void onSaveInstanceState(Bundle outState, PersistableBundle outPersistentState) {
        super.onSaveInstanceState(outState, outPersistentState);
        outState.putParcelableArrayList(STATE_LIST, messageAdapter.getList());
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
    }

    @Override
    protected void onResume() {
        super.onResume();
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
    }

    private void measureTime(){
        final long elapsedTime = (long)((float)(System.currentTimeMillis() - startTime)/1000);
        Bundle params = new Bundle();
        params.putString("feature_name",FEATURE_NAME);
        params.putString("user_id", userID);
        params.putLong("time",elapsedTime);
        mFirebaseAnalytics.logEvent("activity_usage", params);
        FlurryAgent.endTimedEvent("Usage");

        Usage newUsage = new Usage(9,userID,FEATURE_NAME,elapsedTime);
        sendUsage(newUsage);
    }

    public void sendUsage(Usage usage) {
        mAPIService.saveUsage(usage).enqueue(new Callback<Usage>() {
            @Override
            public void onResponse(Call<Usage> call, Response<Usage> response) {

                if(response.isSuccessful()) {
                    Log.i(TAG, "post submitted to API." + response.body().toString());
                }
            }

            @Override
            public void onFailure(Call<Usage> call, Throwable t) {
                Log.e(TAG, "Unable to submit post to API.");
            }
        });
    }

    public void sendChatUsage(ChatUsage usage) {
        mAPIService.saveChat(usage).enqueue(new Callback<ChatUsage>() {
            @Override
            public void onResponse(Call<ChatUsage> call, Response<ChatUsage> response) {

                if(response.isSuccessful()) {
                    Log.i(TAG, "post submitted to API." + response.body().toString());
                }
            }

            @Override
            public void onFailure(Call<ChatUsage> call, Throwable t) {
                Log.e(TAG, "Unable to submit post to API.");
            }
        });
    }

    @Override
    public void OnSpeechRecognitionStarted() {
        et_tts.setEnabled(false);
        et_tts.getText().clear();
        et_tts.setText("Escuchando...");
        btn_tts.setClickable(false);
    }

    @Override
    public void OnSpeechRecognitionStopped() {
    }

    @Override
    public void OnSpeechRecognitionFinalResult(String s) {
        //triggered when SpeechRecognition is done listening.
        //it returns the translated text from the voice input
        final ChatMessage message = new ChatMessage(ChatMessage.MSG_TYPE_RECEIVED, s);
        messageAdapter.add(message);
        et_tts.setEnabled(true);
        et_tts.getText().clear();
        btn_tts.setClickable(true);
        final long elapsedTime = System.currentTimeMillis() - startTimeSST;
        ChatUsage usage = new ChatUsage(9,userID,"STT",elapsedTime,s.length());
        sendChatUsage(usage);
    }
    @Override
    public void OnSpeechRecognitionCurrentResult(String s) {
        //this is called multiple times when SpeechRecognition is
        //still listening. It returns each recognized word when the user is still speaking
        et_tts.setText(s);
    }
    @Override
    public void OnSpeechRecognitionError(int i, String s) {
        Log.e(TAG,s);
        Toast.makeText(this,"No se reconoce palabras", Toast.LENGTH_SHORT);
        et_tts.setEnabled(true);
        et_tts.getText().clear();
        btn_tts.setClickable(true);
    }

    private void checkPermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (!(ContextCompat.checkSelfPermission(this, Manifest.permission.RECORD_AUDIO) == PackageManager.PERMISSION_GRANTED)) {
                Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS,
                        Uri.parse("package:" + getPackageName()));
                startActivity(intent);
                finish();
            }
        }
    }

    @Override
    public boolean onSupportNavigateUp() {
        measureTime();
        onBackPressed();
        finish();
        return true;
    }
}
