package com.uniandes.mobiles.sli.model;

import com.google.gson.annotations.SerializedName;

public class ASLDetection {
    @SerializedName("id")
    private Integer id;
    @SerializedName("asl_user")
    private String asl_user;
    @SerializedName("asl_class")
    private String asl_class;
    @SerializedName("asl_time")
    private long asl_time;
    @SerializedName("asl_prediction")
    private float asl_prediction;

    public ASLDetection(Integer id, String asl_user, String asl_class, long asl_time, float asl_prediction) {
        this.id = id;
        this.asl_user = asl_user;
        this.asl_class = asl_class;
        this.asl_time = asl_time;
        this.asl_prediction = asl_prediction;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getAsl_user() {
        return asl_user;
    }

    public void setAsl_user(String asl_user) {
        this.asl_user = asl_user;
    }

    public String getAsl_class() {
        return asl_class;
    }

    public void setAsl_class(String asl_class) {
        this.asl_class = asl_class;
    }

    public long getAsl_time() {
        return asl_time;
    }

    public void setAsl_time(long asl_time) {
        this.asl_time = asl_time;
    }

    public float getAsl_prediction() {
        return asl_prediction;
    }

    public void setAsl_prediction(float asl_prediction) {
        this.asl_prediction = asl_prediction;
    }
}
