package com.uniandes.mobiles.sli;


import android.database.Cursor;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.MediaController;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;

import com.bumptech.glide.Glide;
import com.google.android.material.appbar.CollapsingToolbarLayout;
import com.uniandes.mobiles.sli.data.DictionaryDBHelper;
import com.uniandes.mobiles.sli.data.DictionaryEntry;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;


/**
 * A simple {@link Fragment} subclass.
 * Use the {@link EntryDetailFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class EntryDetailFragment extends Fragment {

    private static final String ARG_ENTRY_ID = "ID";
    private String mEntryId;

    @BindView(R.id.tv_entry_word) TextView mWord;
    @BindView(R.id.tv_entry_definition) TextView mDefinition;
    @BindView(R.id.tv_entry_example) TextView mExample;
    @BindView(R.id.videoSign) VideoView mSignView;
    @BindView(R.id.videoDefinition) VideoView mDefinitionView;
    @BindView(R.id.videoExample) VideoView mExampleView;

    private DictionaryDBHelper mDictionaryDBHelper;
    private boolean bVideoSignIsBeingTouched = false;
    private boolean bVideoDefIsBeingTouched = false;
    private boolean bVideoExampleIsBeingTouched = false;
    private Handler mHandler = new Handler();

    private Unbinder unbinder;

    public EntryDetailFragment() {
        // Required empty public constructor
    }

    // TODO: Rename and change types and number of parameters
    public static EntryDetailFragment newInstance(String entryId) {
        EntryDetailFragment fragment = new EntryDetailFragment();
        Bundle args = new Bundle();
        args.putString(ARG_ENTRY_ID, entryId);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mEntryId = getArguments().getString(ARG_ENTRY_ID);
        }
    }

    private void loadEntry() {
        new GetEntryByIdTask().execute();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_entry_detail, container, false);
        unbinder= ButterKnife.bind(this, root);
        mDictionaryDBHelper = new DictionaryDBHelper(getActivity());
        loadEntry();
        return root;
    }

    private void showEntry(DictionaryEntry entry) {
       try {
           mWord.setText(entry.getWord());
           mWord.requestFocus();
           mDefinition.setText(entry.getDefinition());
           mExample.setText(entry.getExample());

           mSignView.setVideoURI(Uri.parse(entry.getSignUrl()));
           mSignView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
               @Override
               public void onPrepared(MediaPlayer mediaPlayer) {
                   mSignView.start();
               }
           });
           mSignView.setOnTouchListener(new View.OnTouchListener() {
               @Override
               public boolean onTouch(View view, MotionEvent motionEvent) {
                   if (!bVideoSignIsBeingTouched) {
                       bVideoSignIsBeingTouched = true;
                       mExampleView.pause();
                       mDefinitionView.pause();
                       if (mSignView.isPlaying()) {
                           mSignView.pause();
                       } else {
                           mSignView.resume();
                       }
                       mHandler.postDelayed(new Runnable() {
                           public void run() {
                               bVideoSignIsBeingTouched = false;
                           }
                       }, 100);
                   }
                   return true;
               }
           });

           mDefinitionView.setVideoURI(Uri.parse(entry.getDefinitionUrl()));
           mDefinitionView.setOnTouchListener(new View.OnTouchListener() {
               @Override
               public boolean onTouch(View view, MotionEvent motionEvent) {
                   if (!bVideoDefIsBeingTouched) {
                       bVideoDefIsBeingTouched = true;
                       mSignView.pause();
                       mExampleView.pause();
                       if (mDefinitionView.isPlaying()) {
                           mDefinitionView.pause();
                       } else {
                           mDefinitionView.start();
                           mDefinitionView.resume();
                       }
                       mHandler.postDelayed(new Runnable() {
                           public void run() {
                               bVideoDefIsBeingTouched = false;
                           }
                       }, 100);
                   }
                   return true;
               }
           });

           mExampleView.setVideoURI(Uri.parse(entry.getExampleUrl()));
           mExampleView.setOnTouchListener(new View.OnTouchListener() {
               @Override
               public boolean onTouch(View view, MotionEvent motionEvent) {
                   if (!bVideoExampleIsBeingTouched) {
                       bVideoExampleIsBeingTouched = true;
                       mSignView.pause();
                       mDefinitionView.pause();
                       if (mExampleView.isPlaying()) {
                           mExampleView.pause();
                       } else {
                           mExampleView.start();
                           mExampleView.resume();
                       }
                       mHandler.postDelayed(new Runnable() {
                           public void run() {
                               bVideoExampleIsBeingTouched = false;
                           }
                       }, 100);
                   }
                   return true;
               }
           });
       }
       catch (Exception e){
           Log.e("Fragment",e.getLocalizedMessage());
           showLoadError();
       }

    }

    private void showLoadError() {
        Toast.makeText(getActivity(),
                "Error al cargar información", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onStop() {
        super.onStop();
        if(mSignView!=null){
            mSignView.suspend();
        }
        if(mExampleView!=null){
            mExampleView.suspend();
        }
        if(mDefinitionView!=null){
            mDefinitionView.suspend();
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if(mSignView!=null){
            mSignView.stopPlayback();
        }
        if(mExampleView!=null){
            mExampleView.stopPlayback();
        }
        if(mDefinitionView!=null){
            mDefinitionView.stopPlayback();
        }
        unbinder.unbind();
    }

    private class GetEntryByIdTask extends AsyncTask<Void, Void, Cursor> {

        @Override
        protected Cursor doInBackground(Void... voids) {
            return mDictionaryDBHelper.getEntryById(mEntryId);
        }

        @Override
        protected void onPostExecute(Cursor cursor) {
            if (cursor != null && cursor.moveToLast()) {
                Log.i("Cursor",cursor.toString());
                showEntry(new DictionaryEntry(cursor));
                cursor.close();
            } else {
                showLoadError();
            }
        }

    }

}
