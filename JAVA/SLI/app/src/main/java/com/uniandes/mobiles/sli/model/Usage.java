package com.uniandes.mobiles.sli.model;

import com.google.gson.annotations.SerializedName;

public class Usage {
    @SerializedName("id")
    private Integer id;
    @SerializedName("usage_user")
    private String usage_user;
    @SerializedName("usage_feature")
    private String usage_feature;
    @SerializedName("usage_time")
    private long usage_time;

    public Usage(Integer id, String usage_user, String usage_feature, long usage_time) {
        this.id = id;
        this.usage_user = usage_user;
        this.usage_feature = usage_feature;
        this.usage_time = usage_time;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUsage_user() {
        return usage_user;
    }

    public void setUsage_user(String usage_user) {
        this.usage_user = usage_user;
    }

    public String getUsage_feature() {
        return usage_feature;
    }

    public void setUsage_feature(String usage_feature) {
        this.usage_feature = usage_feature;
    }

    public long getUsage_time() {
        return usage_time;
    }

    public void setUsage_time(long usage_time) {
        this.usage_time = usage_time;
    }
}
