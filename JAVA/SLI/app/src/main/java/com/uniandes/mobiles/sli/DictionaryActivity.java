package com.uniandes.mobiles.sli;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;

import com.flurry.android.FlurryAgent;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.firebase.auth.FirebaseUser;
import com.treebo.internetavailabilitychecker.InternetConnectivityListener;
import com.uniandes.mobiles.sli.model.Usage;
import com.uniandes.mobiles.sli.network.APIService;
import com.uniandes.mobiles.sli.network.APIUtils;

import java.util.HashMap;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DictionaryActivity extends AppCompatActivity implements InternetConnectivityListener {

    public static final String EXTRA_ENTRY_ID = "ENTRY_ID";
    public static final String EXTRA_ENTRY_WORD = "ENTRY_WORD";
    public static final String FEATURE_NAME = "Dictionary";
    public static final String TAG = "DictionaryActivity";

    private FirebaseAnalytics mFirebaseAnalytics;
    private String userID;
    private long startTime;
    private  Map<String, String> usageParams;
    private APIService mAPIService;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dictionary);
        mAPIService = APIUtils.getAPIService();
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);
        userID = getIntent().getStringExtra(LoginActivity.EXTRA_USER_ID);
        EntryFragment fragment = (EntryFragment)
                getSupportFragmentManager().findFragmentById(R.id.entries_container);

        if (fragment == null) {
            fragment = EntryFragment.newInstance();
            fragment.setUserID(userID);
            getSupportFragmentManager()
                    .beginTransaction()
                    .add(R.id.entries_container, fragment)
                    .commit();
        }


    }

    @Override
    protected void onStart() {
        super.onStart();
        startTime = System.currentTimeMillis();
        usageParams = new HashMap<String, String>();
        usageParams.put("FeatureName", FEATURE_NAME);
        usageParams.put("UserID", userID);
        FlurryAgent.logEvent("Usage", usageParams,true);
    }

    @Override
    protected void onStop() {
        measureTime();
        super.onStop();
    }

    @Override
    public boolean onSupportNavigateUp() {
        measureTime();
        onBackPressed();
        finish();
        return true;
    }

    private void measureTime(){
        final long elapsedTime = (long)((float)(System.currentTimeMillis() - startTime)/1000);
        Bundle params = new Bundle();
        params.putString("feature_name",FEATURE_NAME);
        params.putString("user_id", userID);
        params.putDouble("time",elapsedTime);
        mFirebaseAnalytics.logEvent("activity_usage", params);
        FlurryAgent.endTimedEvent("Usage");

        Usage newUsage = new Usage(9,userID,FEATURE_NAME,elapsedTime);
        sendUsage(newUsage);
    }

    public void sendUsage(Usage usage) {
        mAPIService.saveUsage(usage).enqueue(new Callback<Usage>() {
            @Override
            public void onResponse(Call<Usage> call, Response<Usage> response) {

                if(response.isSuccessful()) {
                    Log.i(TAG, "post submitted to API." + response.body().toString());
                }
            }

            @Override
            public void onFailure(Call<Usage> call, Throwable t) {
                Log.e(TAG, "Unable to submit post to API.");
            }
        });
    }

    @Override
    public void onInternetConnectivityChanged(boolean isConnected) {

    }
}
