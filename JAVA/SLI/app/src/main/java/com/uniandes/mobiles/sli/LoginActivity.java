package com.uniandes.mobiles.sli;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.flurry.android.FlurryAgent;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.GoogleAuthProvider;
import com.treebo.internetavailabilitychecker.InternetAvailabilityChecker;
import com.treebo.internetavailabilitychecker.InternetConnectivityListener;

import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;

public class LoginActivity extends AppCompatActivity implements
        View.OnClickListener, InternetConnectivityListener {

    public static final String EXTRA_USER_ID = "USER_ID";
    private final static String TAG = "SLI.LoginActivity";
    private static final int RC_SIGN_IN = 9001;


    @BindView(R.id.et_login_email) EditText emailText;
    @BindView(R.id.et_login_password) EditText passwordText;
    @BindView(R.id.button_signin_google) SignInButton btnLoginGoogle;
    @BindView(R.id.txtRegister)TextView btnSignUp;
    @BindView(R.id.button_signin) Button btnLogin;
    @BindView(R.id.txtAnonimous)TextView btnAnon;
    @BindView(R.id.progressBar) ProgressBar progressBar;

    private FirebaseAuth mAuth;
    private FirebaseAuth.AuthStateListener mAuthListner;
    private FirebaseAnalytics mFirebaseAnalytics;
    private GoogleSignInClient mGoogleSignInClient;
    private InternetAvailabilityChecker mInternetAvailabilityChecker;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mAuth = FirebaseAuth.getInstance();
        mFirebaseAnalytics  = FirebaseAnalytics.getInstance(this);
        mInternetAvailabilityChecker = InternetAvailabilityChecker.getInstance();

        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);

        btnLoginGoogle.setOnClickListener(this);
        btnSignUp.setOnClickListener(this);
        btnAnon.setOnClickListener(this);
        btnLogin.setOnClickListener(this);

        mAuthListner = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                if (firebaseAuth.getCurrentUser() != null) {
                    updateUI(firebaseAuth.getCurrentUser());
                }
            }
        };

        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(R.string.default_web_client_id))
                .requestEmail()
                .build();
        mGoogleSignInClient = GoogleSignIn.getClient(this, gso);
    }

    @Override
    protected void onStart() {
        super.onStart();
        mAuth.addAuthStateListener(mAuthListner);
        mInternetAvailabilityChecker.addInternetConnectivityListener(this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (mAuthListner != null) {
            mAuth.removeAuthStateListener(mAuthListner);
        }
        if(mInternetAvailabilityChecker != null){
            mInternetAvailabilityChecker.removeInternetConnectivityChangeListener(this);
        }
    }

    private void signIn() {
        Intent signInIntent = mGoogleSignInClient.getSignInIntent();
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        // Result returned from launching the Intent from GoogleSignInApi.getSignInIntent(...);
        if (requestCode == RC_SIGN_IN) {
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            try {
                // Google Sign In was successful, authenticate with Firebase
                GoogleSignInAccount account = task.getResult(ApiException.class);
                firebaseAuthWithGoogle(account);
            } catch (ApiException e) {
                // Google Sign In failed, update UI appropriately
                Log.w(TAG, "Google sign in failed", e);
            }
        }
    }

    private void firebaseAuthWithGoogle(final GoogleSignInAccount account) {
        AuthCredential credential = GoogleAuthProvider.getCredential(account.getIdToken(), null);
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            Bundle bundle = new Bundle();
                            bundle.putString(FirebaseAnalytics.Param.METHOD, "google");
                            mFirebaseAnalytics.logEvent(FirebaseAnalytics.Event.LOGIN, bundle);
                            Log.d(TAG, "signInWithCredential:success");
                            FirebaseUser user = mAuth.getCurrentUser();
                            updateUI(user);
                        } else {
                            // If sign in fails, display a message to the user.
                            Log.w(TAG, "signInWithCredential:failure", task.getException());
                            Toast.makeText(LoginActivity.this, "Aut Fail", Toast.LENGTH_SHORT).show();
                            updateUI(null);
                        }
                    }
                });
    }


    private void updateUI(FirebaseUser user) {

        if (user != null) {
            final Intent activityIntent = new Intent(this, MainActivity.class);
            activityIntent.putExtra(EXTRA_USER_ID,user.getUid());
            activityIntent.putExtra("UserName",user.getDisplayName());

            startActivity(activityIntent);
            finish();
        } else {
            Toast.makeText(LoginActivity.this, "Fallo la autenticacion",
                    Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onClick(View view) {
        int i = view.getId();
        if (i == R.id.txtRegister) {
            startActivity(new Intent(LoginActivity.this, RegisterActivity.class));
            finish();
        } else if(i==R.id.txtAnonimous) {
            final Intent anon = new Intent(LoginActivity.this, MainActivity.class);

            Bundle bundle = new Bundle();
            bundle.putString(FirebaseAnalytics.Param.METHOD, "anonymous");
            mFirebaseAnalytics.logEvent(FirebaseAnalytics.Event.LOGIN, bundle);

            Map<String, String> loginParams = new HashMap<String, String>();
            loginParams.put("Method", "anonymous");
            FlurryAgent.logEvent("Login", loginParams);

            anon.putExtra(EXTRA_USER_ID,"002");
            startActivity(anon);
            finish();
        }
        else if (i == R.id.button_signin) {
            final String email = emailText.getText().toString();
            final String password = passwordText.getText().toString();
            if (TextUtils.isEmpty(email)) {
                Toast.makeText(getApplicationContext(), "Ingrese el correo", Toast.LENGTH_SHORT).show();
                return;
            }
            if (TextUtils.isEmpty(password)) {
                Toast.makeText(getApplicationContext(), "Ingrese la contraseña", Toast.LENGTH_SHORT).show();
                return;
            }
            progressBar.setVisibility(View.VISIBLE);
            //authenticate user
            mAuth.signInWithEmailAndPassword(email, password)
                    .addOnCompleteListener(LoginActivity.this, new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {
                            progressBar.setVisibility(View.GONE);
                            if (task.isSuccessful()) {
                                // there was an error
                                Log.d(TAG, "signInWithEmail:success");

                                Bundle bundle = new Bundle();
                                bundle.putString(FirebaseAnalytics.Param.METHOD, "email");
                                mFirebaseAnalytics.logEvent(FirebaseAnalytics.Event.LOGIN, bundle);

                                Map<String, String> loginParams = new HashMap<String, String>();
                                loginParams.put("Method", "email");
                                FirebaseUser user = mAuth.getCurrentUser();
                                loginParams.put("UserID", user.getUid());
                                FlurryAgent.logEvent("Login", loginParams);

                                updateUI(user);
                            } else {
                                Log.d(TAG, "singInWithEmail:Fail");
                                updateUI(null);
                            }
                        }
                    });
        } else if (i == R.id.button_signin_google) {
            signIn();
        }
    }

    @Override
    public void onInternetConnectivityChanged(boolean isConnected) {
        if(isConnected){
            btnLogin.setClickable(true);
            btnSignUp.setClickable(true);
        }else{
            Toast.makeText(getApplicationContext(), "No hay conexion. Puedes usar la aplicacion sin cuenta", Toast.LENGTH_LONG).show();
            btnLogin.setClickable(false);
            btnSignUp.setClickable(false);
        }
    }
}
