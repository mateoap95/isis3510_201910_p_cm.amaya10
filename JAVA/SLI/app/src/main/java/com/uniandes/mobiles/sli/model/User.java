package com.uniandes.mobiles.sli.model;

import com.google.gson.annotations.SerializedName;

import java.util.Date;

public class User {

    @SerializedName("id")
    private Integer id;
    @SerializedName("user_name")
    private String user_name;
    @SerializedName("user_email")
    private String user_email;
    @SerializedName("user_uid")
    private String user_uid;
    @SerializedName("user_lastname")
    private String user_lastname;
    @SerializedName("user_condition")
    private String user_condition;
    @SerializedName("user_sex")
    private String user_sex;
    @SerializedName("user_birthdate")
    private Date user_birthdate;

    public User(Integer id, String user_name, String user_email, String user_uid, String user_lastname, String user_condition, String user_sex, Date user_birthdate) {
        this.id = id;
        this.user_name = user_name;
        this.user_email = user_email;
        this.user_uid = user_uid;
        this.user_lastname = user_lastname;
        this.user_condition = user_condition;
        this.user_sex = user_sex;
        this.user_birthdate = user_birthdate;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUser_name() {
        return user_name;
    }

    public void setUser_name(String user_name) {
        this.user_name = user_name;
    }

    public String getUser_email() {
        return user_email;
    }

    public void setUser_email(String user_email) {
        this.user_email = user_email;
    }

    public String getUser_uid() {
        return user_uid;
    }

    public void setUser_uid(String user_uid) {
        this.user_uid = user_uid;
    }

    public String getUser_lastname() {
        return user_lastname;
    }

    public void setUser_lastname(String user_lastname) {
        this.user_lastname = user_lastname;
    }

    public String getUser_condition() {
        return user_condition;
    }

    public void setUser_condition(String user_condition) {
        this.user_condition = user_condition;
    }

    public String getUser_sex() {
        return user_sex;
    }

    public void setUser_sex(String user_sex) {
        this.user_sex = user_sex;
    }

    public Date getUser_birthdate() {
        return user_birthdate;
    }

    public void setUser_birthdate(Date user_birthdate) {
        this.user_birthdate = user_birthdate;
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", user_name='" + user_name + '\'' +
                ", user_email='" + user_email + '\'' +
                ", user_uid='" + user_uid + '\'' +
                ", user_lastname='" + user_lastname + '\'' +
                ", user_condition='" + user_condition + '\'' +
                ", user_sex='" + user_sex + '\'' +
                ", user_birthdate=" + user_birthdate +
                '}';
    }
}
