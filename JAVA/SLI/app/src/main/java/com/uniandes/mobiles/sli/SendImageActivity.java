package com.uniandes.mobiles.sli;

import androidx.appcompat.app.AppCompatActivity;

import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.flurry.android.FlurryAgent;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.firebase.auth.FirebaseUser;
import com.uniandes.mobiles.sli.model.Survey;
import com.uniandes.mobiles.sli.model.Usage;
import com.uniandes.mobiles.sli.network.APIService;
import com.uniandes.mobiles.sli.network.APIUtils;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SendImageActivity extends AppCompatActivity {

    public static final String TAG = "SurveyActivity";

    @BindView(R.id.imageViewSign)
    ImageView signImage;
    @BindView(R.id.imageViewPicture)
    ImageView currentImage;
    @BindView(R.id.buttonYes)
    Button btn_yes;
    @BindView(R.id.buttonNo)
    Button btn_no;

    private String Uid;
    private String detectedClass;
    private String path;
    private String model;
    private Float confidence;
    private FirebaseAnalytics mFirebaseAnalytics;
    private APIService mAPIService;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_send_image);
        ButterKnife.bind(this);

        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);
        detectedClass = getIntent().getStringExtra("class");
        path = getIntent().getStringExtra("name");
        model = getIntent().getStringExtra("model");
        Uid = getIntent().getStringExtra("userid");
        confidence = getIntent().getFloatExtra("confidence",0);

        mAPIService = APIUtils.getAPIService();

        if (detectedClass != null) {
            if(model.contains("CSL")){
                Glide
                        .with(this)
                        .load(Uri.parse("file:///android_asset/letra" + detectedClass + ".jpg"))
                        .error(R.drawable.ic_sign_language)
                        .into(signImage);
            }
           else{
                Glide
                        .with(this)
                        .load(Uri.parse("file:///android_asset/" + detectedClass.toUpperCase() + "_test.jpg"))
                        .error(R.drawable.ic_sign_language)
                        .into(signImage);
            }
        }
        if (path != null) {
            Glide
                    .with(this)
                    .load(Uri.fromFile(new File(path)))
                    .into(currentImage);
        }

        btn_yes.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                Map<String, String> surveyParams = new HashMap<String, String>();
                surveyParams.put("Correct", "YES");
                surveyParams.put("Class", detectedClass);
                surveyParams.put("UserID", Uid);
                surveyParams.put("Model", model);
                surveyParams.put("Confidence", Float.toString(confidence));
                FlurryAgent.logEvent("Survey", surveyParams);

                Bundle params = new Bundle();
                params.putInt("correct", 1);
                params.putString("class", detectedClass);
                params.putString("model", model);
                params.putFloat("confidence", confidence);
                mFirebaseAnalytics.logEvent("Survey", params);

                Survey survey = new Survey(9,Uid,detectedClass,1,confidence,model);
                sendSurvey(survey);


                finish();
            }
        });

        btn_no.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Map<String, String> surveyParams = new HashMap<String, String>();
                surveyParams.put("Correct", "NO");
                surveyParams.put("Class", detectedClass);
                surveyParams.put("UserID", Uid);
                surveyParams.put("Model", model);
                surveyParams.put("Confidence", Float.toString(confidence));
                FlurryAgent.logEvent("Survey", surveyParams);

                Bundle params = new Bundle();
                params.putInt("correct", 0);
                params.putString("class", detectedClass);
                params.putString("model", model);
                params.putFloat("confidence", confidence);
                mFirebaseAnalytics.logEvent("Survey", params);

                Survey survey = new Survey(9,Uid,detectedClass,0,confidence,model);
                sendSurvey(survey);

                finish();
            }
        });

    }

    public void sendSurvey(Survey survey) {
        mAPIService.saveSurvey(survey).enqueue(new Callback<Survey>() {
            @Override
            public void onResponse(Call<Survey> call, Response<Survey> response) {

                if (response.isSuccessful()) {
                    Log.i(TAG, "post submitted to API." + response.body().toString());
                }
            }

            @Override
            public void onFailure(Call<Survey> call, Throwable t) {
                Log.e(TAG, "Unable to submit post to API.");
            }
        });
    }
}
