package com.uniandes.mobiles.sli.model;

import com.google.gson.annotations.SerializedName;

public class ChatUsage {
    @SerializedName("id")
    private Integer id;
    @SerializedName("chat_user")
    private String chat_user;
    @SerializedName("chat_method")
    private String chat_method;
    @SerializedName("chat_time")
    private long chat_time;
    @SerializedName("chat_lenght")
    private int chat_lenght;

    public ChatUsage(Integer id, String chat_user, String chat_method, long chat_time, int chat_lenght) {
        this.id = id;
        this.chat_user = chat_user;
        this.chat_method = chat_method;
        this.chat_time = chat_time;
        this.chat_lenght = chat_lenght;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getChat_user() {
        return chat_user;
    }

    public void setChat_user(String chat_user) {
        this.chat_user = chat_user;
    }

    public String getChat_method() {
        return chat_method;
    }

    public void setChat_method(String chat_method) {
        this.chat_method = chat_method;
    }

    public long getChat_time() {
        return chat_time;
    }

    public void setChat_time(long chat_time) {
        this.chat_time = chat_time;
    }

    public int getChat_lenght() {
        return chat_lenght;
    }

    public void setChat_lenght(int chat_lenght) {
        this.chat_lenght = chat_lenght;
    }
}
