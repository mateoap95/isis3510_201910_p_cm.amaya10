package com.uniandes.mobiles.sli.data;

import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.uniandes.mobiles.sli.R;
import com.uniandes.mobiles.sli.data.DictionaryContract.DicEntry;

import butterknife.BindView;
import butterknife.ButterKnife;

public class EntriesCursorAdapter extends CursorAdapter {

    @BindView(R.id.tv_name)TextView nameText;
    @BindView(R.id.iv_avatar)ImageView avatarImage;

    public EntriesCursorAdapter(Context context, Cursor c) {
        super(context, c, 0);
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup viewGroup) {
        LayoutInflater inflater = LayoutInflater.from(context);
        return inflater.inflate(R.layout.list_item_entry, viewGroup, false);
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor) {
        // Referencias UI.
        ButterKnife.bind(this, view);

        // Get valores.
        String name = cursor.getString(cursor.getColumnIndex(DicEntry.WORD));
        String avatarUri = cursor.getString(cursor.getColumnIndex(DicEntry.LETTER));

        // Setup.
        nameText.setText(name);
        Glide
                .with(context)
                .load(Uri.parse("file:///android_asset/letra" + avatarUri+".jpg"))
                .error(R.drawable.ic_sign_language)
                .apply(RequestOptions.circleCropTransform())
                .into(avatarImage);
    }

}
