package com.uniandes.mobiles.sli;

import android.os.Bundle;

import com.flurry.android.FlurryAgent;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.treebo.internetavailabilitychecker.InternetConnectivityListener;
import com.uniandes.mobiles.sli.model.EntryVisit;
import com.uniandes.mobiles.sli.model.Usage;
import com.uniandes.mobiles.sli.network.APIService;
import com.uniandes.mobiles.sli.network.APIUtils;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.util.Log;
import android.view.View;

import java.util.HashMap;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class EntryDetailActivity extends AppCompatActivity implements InternetConnectivityListener {

    public static final String FEATURE_NAME = "Entry Visit";
    public static final String TAG = "EntryDetailActivity";

    private FirebaseAnalytics mFirebaseAnalytics;
    private String userID;
    private long startTime;
    private Map<String, String> usageParams;
    private APIService mAPIService;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_entry_detail);

        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);
        String id = getIntent().getStringExtra(DictionaryActivity.EXTRA_ENTRY_ID);
        String word = getIntent().getStringExtra(DictionaryActivity.EXTRA_ENTRY_WORD);
        userID = getIntent().getStringExtra(LoginActivity.EXTRA_USER_ID);

        mAPIService = APIUtils.getAPIService();

        Bundle params = new Bundle();
        params.putString("word", word);
        mFirebaseAnalytics.logEvent("entry_visit", params);

        EntryVisit visit = new EntryVisit(9,userID,word);
        sendEntryVisit(visit);

        EntryDetailFragment fragment = (EntryDetailFragment)
                getSupportFragmentManager().findFragmentById(R.id.entry_detail_container);
        if (fragment == null) {
            fragment = EntryDetailFragment.newInstance(id);
            getSupportFragmentManager()
                    .beginTransaction()
                    .add(R.id.entry_detail_container, fragment)
                    .commit();
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        startTime = System.currentTimeMillis();
        usageParams = new HashMap<String, String>();
        usageParams.put("FeatureName", FEATURE_NAME);
        usageParams.put("UserID", userID);
        FlurryAgent.logEvent("Usage", usageParams,true);
    }

    @Override
    public boolean onSupportNavigateUp() {
        measureTime();
        onBackPressed();
        finish();
        return true;
    }

    @Override
    protected void onStop() {
        measureTime();
        super.onStop();
    }

    private void measureTime() {
        final long elapsedTime = (long)((float)(System.currentTimeMillis() - startTime)/1000);
        Bundle params = new Bundle();
        params.putString("feature_name", FEATURE_NAME);
        params.putString("user_id", userID);
        params.putLong("time", elapsedTime);
        mFirebaseAnalytics.logEvent("activity_usage", params);
        FlurryAgent.endTimedEvent("Usage");

        Usage newUsage = new Usage(9,userID,FEATURE_NAME,elapsedTime);
        sendUsage(newUsage);
    }

    public void sendUsage(Usage usage) {
        mAPIService.saveUsage(usage).enqueue(new Callback<Usage>() {
            @Override
            public void onResponse(Call<Usage> call, Response<Usage> response) {

                if (response.isSuccessful()) {
                    Log.i(TAG, "post submitted to API." + response.body().toString());
                }
            }

            @Override
            public void onFailure(Call<Usage> call, Throwable t) {
                Log.e(TAG, "Unable to submit post to API.");
            }
        });
    }

    public void sendEntryVisit(EntryVisit visit) {
        mAPIService.saveEntry(visit).enqueue(new Callback<EntryVisit>() {
            @Override
            public void onResponse(Call<EntryVisit> call, Response<EntryVisit> response) {

                if (response.isSuccessful()) {
                    Log.i(TAG, "post submitted to API." + response.body().toString());
                }
            }

            @Override
            public void onFailure(Call<EntryVisit> call, Throwable t) {
                Log.e(TAG, "Unable to submit post to API.");
            }
        });
    }

    @Override
    public void onInternetConnectivityChanged(boolean isConnected) {
        if (isConnected) {

        } else {

        }
    }
}
