package com.uniandes.mobiles.sli.data;

import android.content.ContentValues;
import android.database.Cursor;

import com.uniandes.mobiles.sli.data.DictionaryContract.DicEntry;

import java.util.UUID;

public class DictionaryEntry {
    private String id;
    private String word;
    private String definition;
    private String signUrl;
    private String definitionUrl;
    private String example;
    private String exampleUrl;
    private String letter;
    private int cm;
    private String translationUrl;


    public DictionaryEntry(String word, String definition, String signUrl, String definitionUrl, String example, String exampleUrl, String letter, int cm, String translationUrl) {
        this.id = UUID.randomUUID().toString();
        this.word = word;
        this.definition = definition;
        this.signUrl = signUrl;
        this.definitionUrl = definitionUrl;
        this.example = example;
        this.exampleUrl = exampleUrl;
        this.letter = letter;
        this.cm = cm;
        this.translationUrl = translationUrl;
    }

    public DictionaryEntry(Cursor cursor) {
        this.id = cursor.getString(cursor.getColumnIndex(DicEntry.ID));
        this.word = cursor.getString(cursor.getColumnIndex(DicEntry.WORD));
        this.definition = cursor.getString(cursor.getColumnIndex(DicEntry.DEFINITION));
        this.signUrl = cursor.getString(cursor.getColumnIndex(DicEntry.SIGN_URL));
        this.definitionUrl = cursor.getString(cursor.getColumnIndex(DicEntry.DEFINITION_URL));
        this.example = cursor.getString(cursor.getColumnIndex(DicEntry.EXAMPLE));
        this.exampleUrl = cursor.getString(cursor.getColumnIndex(DicEntry.EXAMPLE_URL));
        this.letter = cursor.getString(cursor.getColumnIndex(DicEntry.LETTER));
        this.cm = cursor.getInt(cursor.getColumnIndex(DicEntry.CM));
        this.translationUrl = cursor.getString(cursor.getColumnIndex(DicEntry.TRANSLATION_URL));
    }

    public String getId() {
        return id;
    }

    public String getWord() {
        return word;
    }

    public String getDefinition() {
        return definition;
    }

    public String getSignUrl() {
        return signUrl;
    }

    public String getDefinitionUrl() {
        return definitionUrl;
    }

    public String getExample() {
        return example;
    }

    public String getExampleUrl() {
        return exampleUrl;
    }

    public String getTranslationUrl() {
        return translationUrl;
    }

    public String getLetter() {
        return letter;
    }

    public int getCm() {
        return cm;
    }

    public ContentValues toContentValues() {
        ContentValues values = new ContentValues();
        values.put(DicEntry.ID, id);
        values.put(DicEntry.WORD, word);
        values.put(DicEntry.DEFINITION, definition);
        values.put(DicEntry.SIGN_URL, signUrl);
        values.put(DicEntry.DEFINITION_URL, definitionUrl);
        values.put(DicEntry.EXAMPLE, example);
        values.put(DicEntry.EXAMPLE_URL, exampleUrl);
        values.put(DicEntry.LETTER, letter);
        values.put(DicEntry.CM, cm);
        values.put(DicEntry.TRANSLATION_URL, translationUrl);
        return values;
    }

}
