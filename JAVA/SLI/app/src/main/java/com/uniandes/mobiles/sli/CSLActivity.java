package com.uniandes.mobiles.sli;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.flurry.android.FlurryAgent;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.firebase.auth.FirebaseUser;
import com.uniandes.mobiles.sli.model.CSLDetection;
import com.uniandes.mobiles.sli.model.Usage;
import com.uniandes.mobiles.sli.network.APIService;
import com.uniandes.mobiles.sli.network.APIUtils;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.fotoapparat.Fotoapparat;
import io.fotoapparat.configuration.CameraConfiguration;
import io.fotoapparat.error.CameraErrorListener;
import io.fotoapparat.exception.camera.CameraException;
import io.fotoapparat.parameter.ScaleType;
import io.fotoapparat.preview.Frame;
import io.fotoapparat.preview.FrameProcessor;
import io.fotoapparat.result.BitmapPhoto;
import io.fotoapparat.result.PhotoResult;
import io.fotoapparat.result.WhenDoneListener;
import io.fotoapparat.selector.FocusModeSelectorsKt;
import io.fotoapparat.view.CameraView;
import io.fotoapparat.view.FocusView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static io.fotoapparat.log.LoggersKt.fileLogger;
import static io.fotoapparat.log.LoggersKt.logcat;
import static io.fotoapparat.log.LoggersKt.loggers;
import static io.fotoapparat.result.transformer.ResolutionTransformersKt.scaled;
import static io.fotoapparat.selector.AspectRatioSelectorsKt.standardRatio;
import static io.fotoapparat.selector.FlashSelectorsKt.autoFlash;
import static io.fotoapparat.selector.FlashSelectorsKt.autoRedEye;
import static io.fotoapparat.selector.FlashSelectorsKt.off;
import static io.fotoapparat.selector.FlashSelectorsKt.torch;
import static io.fotoapparat.selector.FocusModeSelectorsKt.autoFocus;
import static io.fotoapparat.selector.FocusModeSelectorsKt.continuousFocusPicture;
import static io.fotoapparat.selector.FocusModeSelectorsKt.fixed;
import static io.fotoapparat.selector.LensPositionSelectorsKt.back;
import static io.fotoapparat.selector.PreviewFpsRangeSelectorsKt.highestFps;
import static io.fotoapparat.selector.ResolutionSelectorsKt.highestResolution;
import static io.fotoapparat.selector.SelectorsKt.firstAvailable;
import static io.fotoapparat.selector.SensorSensitivitySelectorsKt.highestSensorSensitivity;

public class CSLActivity extends AppCompatActivity {

    private static final String FEATURE_NAME = "CSL Detection";

    private static final String TAG = "CSL";
    private static final String MODEL_PATH = "optimized_graph_csl.tflite";
    private static final boolean QUANT = false;
    private static final String LABEL_PATH = "retrained_labels_csl.txt";
    private static final int INPUT_SIZE = 224;

    private Classifier classifier;

    private final PermissionsDelegate permissionsDelegate = new PermissionsDelegate(this);
    private boolean hasCameraPermission;

    private Executor executor = Executors.newSingleThreadExecutor();
    @BindView(R.id.et_Result)
    EditText etResult;
    @BindView(R.id.btnDetectObject)
    Button btnDetectObject;
    @BindView(R.id.btnClear)
    Button btnReset;

    private Fotoapparat fotoapparat;
    @BindView(R.id.cameraView)
    CameraView cameraView;
    @BindView(R.id.focusView)
    FocusView focusView;
    boolean activeCameraBack = true;

    private FirebaseAnalytics mFirebaseAnalytics;
    private String userID;
    private long startTime;
    private Map<String, String> usageParams;
    private APIService mAPIService;

    private CameraConfiguration cameraConfiguration = CameraConfiguration
            .builder()
            .photoResolution(standardRatio(
                    highestResolution()
            ))
            .focusMode(firstAvailable(
                    continuousFocusPicture(),
                    autoFocus(),
                    fixed()
            ))
            .flash(firstAvailable(
                    autoRedEye(),
                    autoFlash(),
                    torch(),
                    off()
            ))
            .previewFpsRange(highestFps())
            .sensorSensitivity(highestSensorSensitivity())
            .frameProcessor(new CSLActivity.SampleFrameProcessor())
            .build();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_csl);
        ButterKnife.bind(this);

        hasCameraPermission = permissionsDelegate.hasCameraPermission();
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);
        userID = getIntent().getStringExtra(LoginActivity.EXTRA_USER_ID);

        mAPIService = APIUtils.getAPIService();

        etResult.setEnabled(false);

        if (hasCameraPermission) {
            cameraView.setVisibility(View.VISIBLE);
        } else {
            permissionsDelegate.requestCameraPermission();
        }

        fotoapparat = createFotoapparat();

        fotoapparat = Fotoapparat
                .with(this)
                .into(cameraView)           // view which will draw the camera preview
                .previewScaleType(ScaleType.CenterCrop)  // we want the preview to fill the view
                .photoResolution(highestResolution())
                .focusMode(firstAvailable(  // (optional) use the first focus mode which is supported by device
                        FocusModeSelectorsKt.continuousFocusPicture(),
                        FocusModeSelectorsKt.autoFocus(),        // in case if continuous focus is not available on device, auto focus will be used
                        FocusModeSelectorsKt.fixed()             // if even auto focus is not available - fixed focus mode will be used
                ))
                .logger(loggers(            // (optional) we want to log camera events in 2 places at once
                        logcat(),           // ... in logcat
                        fileLogger(this)    // ... and to file
                ))
                .build();

        btnDetectObject.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                takePicture();
            }
        });

        btnReset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                etResult.getText().clear();
            }
        });

        initTensorFlowAndLoadModel();
    }

    @Override
    public void onSaveInstanceState(Bundle outState, PersistableBundle outPersistentState) {
        outState.putString("result", etResult.getText().toString());
        super.onSaveInstanceState(outState, outPersistentState);
    }

    private Fotoapparat createFotoapparat() {
        return Fotoapparat
                .with(this)
                .into(cameraView)
                .focusView(focusView)
                .previewScaleType(ScaleType.CenterCrop)
                .lensPosition(back())
                .frameProcessor(new CSLActivity.SampleFrameProcessor())
                .logger(loggers(
                        logcat(),
                        fileLogger(this)
                ))
                .cameraErrorCallback(new CameraErrorListener() {
                    @Override
                    public void onError(CameraException e) {
                        Toast.makeText(CSLActivity.this, e.toString(), Toast.LENGTH_LONG).show();
                    }
                })
                .build();
    }


    private void takePicture() {

        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_.jpg";
        File storageDir = getExternalFilesDir("photos");

        final long startTime = System.currentTimeMillis();
        PhotoResult result = fotoapparat.takePicture();
        File saveFile = new File(storageDir, imageFileName);
        result.saveToFile(saveFile);
        result.toBitmap(scaled(0.25f))
                .whenDone(new WhenDoneListener<BitmapPhoto>() {
                    @Override
                    public void whenDone(BitmapPhoto bitmapPhoto) {
                        if (bitmapPhoto == null) {
                            Log.e(TAG, "Couldn't capture photo.");
                            return;
                        }

                        Bitmap bitmap = bitmapPhoto.bitmap;
                        bitmap = Bitmap.createScaledBitmap(bitmap, INPUT_SIZE, INPUT_SIZE, false);
                        final List<Classifier.Recognition> results =
                                classifier.recognizeImage(bitmap);
                        final long elapsedTime = System.currentTimeMillis() - startTime;
                        postResults(results,elapsedTime,saveFile.getAbsolutePath());
                    }
                });
    }

    private void postResults(List<Classifier.Recognition> results, long time, String path){
        final Random rand = new Random();
        final float alpha = rand.nextFloat();
        boolean letter = false;
        final String class_label = results.get(0).getTitle();
        if (class_label.contains("nothing")) {
            Toast.makeText(this, "No se detecto ninguna seña", Toast.LENGTH_SHORT);
        } else if (class_label.contains("del")) {

        } else if (class_label.contains("space")) {
            etResult.append(" ");
        } else {
            etResult.append(class_label);
            letter = true;
        }

        float confidence = results.get(0).getConfidence();
        Bundle params = new Bundle();
        params.putString("class_name", class_label);
        params.putFloat("precision", confidence);
        params.putLong("time", time);
        mFirebaseAnalytics.logEvent("detect_csl", params);

        Map<String, String> cslParams = new HashMap<String, String>();
        cslParams.put("ClassName", class_label);
        cslParams.put("Precision", Float.toString(confidence));
        cslParams.put("Time", Long.toString(time));
        FlurryAgent.logEvent("CSL_Detect", cslParams);

        CSLDetection detection = new CSLDetection(9,userID,class_label,time,confidence);
        sendDetection(detection);

        Log.i("ResultsCSL", results.toString());

        if(alpha < 0.2 && letter){
            final Intent activityIntent = new Intent(this, SendImageActivity.class);
            activityIntent.putExtra("class",class_label);
            activityIntent.putExtra("name",path);
            activityIntent.putExtra("userid",userID);
            activityIntent.putExtra("model","CSL");
            activityIntent.putExtra("confidence",results.get(0).getConfidence());
            startActivity(activityIntent);
        }
    }


    @Override
    protected void onResume() {
        super.onResume();
        startTime = System.currentTimeMillis();
        usageParams = new HashMap<String, String>();
        usageParams.put("FeatureName", FEATURE_NAME);
        usageParams.put("UserID", userID);
        FlurryAgent.logEvent("Usage", usageParams, true);
        fotoapparat.start();
    }

    @Override
    protected void onPause() {
        measureTime();
        super.onPause();
        fotoapparat.stop();
    }

    private void measureTime() {
        final long elapsedTime = (long)((float)(System.currentTimeMillis() - startTime)/1000);
        Bundle params = new Bundle();
        params.putString("feature_name", FEATURE_NAME);
        params.putString("user_id", userID);
        params.putLong("time", elapsedTime);
        mFirebaseAnalytics.logEvent("activity_usage", params);
        FlurryAgent.endTimedEvent("Usage");

        Usage newUsage = new Usage(9,userID,FEATURE_NAME,elapsedTime);
        sendUsage(newUsage);
    }

    public void sendUsage(Usage usage) {
        mAPIService.saveUsage(usage).enqueue(new Callback<Usage>() {
            @Override
            public void onResponse(Call<Usage> call, Response<Usage> response) {

                if (response.isSuccessful()) {
                    Log.i(TAG, "post submitted to API." + response.body().toString());
                }
            }

            @Override
            public void onFailure(Call<Usage> call, Throwable t) {
                Log.e(TAG, "Unable to submit post to API.");
            }
        });
    }

    public void sendDetection(CSLDetection detection) {
        mAPIService.saveCSLDetection(detection).enqueue(new Callback<CSLDetection>() {
            @Override
            public void onResponse(Call<CSLDetection> call, Response<CSLDetection> response) {

                if (response.isSuccessful()) {
                    Log.i(TAG, "post submitted to API." + response.body().toString());
                }
            }

            @Override
            public void onFailure(Call<CSLDetection> call, Throwable t) {
                Log.e(TAG, "Unable to submit post to API.");
            }
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        executor.execute(new Runnable() {
            @Override
            public void run() {
                classifier.close();
            }
        });
    }

    private void initTensorFlowAndLoadModel() {
        executor.execute(new Runnable() {
            @Override
            public void run() {
                try {
                    classifier = TensorFlowImageClassifier.create(
                            getAssets(),
                            MODEL_PATH,
                            LABEL_PATH,
                            INPUT_SIZE,
                            QUANT);
                    makeButtonVisible();
                } catch (final Exception e) {
                    throw new RuntimeException("Error initializing TensorFlow!", e);
                }
            }
        });
    }

    private void makeButtonVisible() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                btnDetectObject.setVisibility(View.VISIBLE);
            }
        });
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (permissionsDelegate.resultGranted(requestCode, permissions, grantResults)) {
            hasCameraPermission = true;
            fotoapparat.start();
            cameraView.setVisibility(View.VISIBLE);
        }
    }

    private class SampleFrameProcessor implements FrameProcessor {
        @Override
        public void process(Frame frame) {
            // Perform frame processing, if needed
        }
    }

    @Override
    public boolean onSupportNavigateUp() {
        measureTime();
        onBackPressed();
        finish();
        return true;
    }
}
