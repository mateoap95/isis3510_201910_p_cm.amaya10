package com.uniandes.mobiles.sli.network;

import com.uniandes.mobiles.sli.model.ASLDetection;
import com.uniandes.mobiles.sli.model.CSLDetection;
import com.uniandes.mobiles.sli.model.ChatUsage;
import com.uniandes.mobiles.sli.model.EntryVisit;
import com.uniandes.mobiles.sli.model.Survey;
import com.uniandes.mobiles.sli.model.Usage;
import com.uniandes.mobiles.sli.model.User;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

public interface APIService {
    @POST("/api/user")
    Call<User> saveUser(@Body User user);

    @POST("/api/usage")
    Call<Usage> saveUsage(@Body Usage usage);

    @POST("/api/csl")
    Call<CSLDetection> saveCSLDetection(@Body CSLDetection detection);

    @POST("/api/asl")
    Call<ASLDetection> saveASLDetection(@Body ASLDetection detection);

    @POST("/api/survey")
    Call<Survey> saveSurvey(@Body Survey survey);

    @POST("/api/entry")
    Call<EntryVisit> saveEntry(@Body EntryVisit entry);

    @POST("/api/chat")
    Call<ChatUsage> saveChat(@Body ChatUsage chat);
}
