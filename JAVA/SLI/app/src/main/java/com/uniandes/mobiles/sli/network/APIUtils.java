package com.uniandes.mobiles.sli.network;

public class APIUtils {
    private APIUtils() {}

    public static APIService getAPIService() {

        return RetroFitClientInstance.getRetrofitInstance().create(APIService.class);
    }
}
